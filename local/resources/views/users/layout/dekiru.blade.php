@extends ('users.layout.user')
@section ('content')
<div class="container">

  <br>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link dekiru" id="vocabulary" href="{{ url('users/dekiru/vocabulary') }}">Từ Vựng</a>
    </li>
    <li class="nav-item">
      <a class="nav-link dekiru" id="grammar" href="{{ url('users/dekiru/grammar') }}">Ngữ Pháp</a>
    </li>
    <li class="nav-item">
      <a class="nav-link dekiru" id="kanji" href="{{ url('users/dekiru/kanji') }}">Kanji</a>
    </li>
    <li class="nav-item">
      <a class="nav-link dekiru" id="listen" href="{{ url('users/dekiru/kanji') }}">Nghe</a>
    </li>
    <li class="nav-item">
      <a class="nav-link dekiru" id="speak" href="{{ url('users/dekiru/kanji') }}">Nói</a>
    </li>
  </ul>

  @yield('dekiru')
</div>

@endsection
