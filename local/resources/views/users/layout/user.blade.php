<!DOCTYPE HTML>
<html>
<head>
<title>Home</title>
<!-- <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet"> -->
<script src="{{ asset ('js/jquery-3.3.1.min.js') }}"></script> 
<script src="{{ asset ('js/bootstrap.min.js') }}"></script> 
<link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset ('css/style.css') }}" rel="stylesheet" type="text/css">
<script src="https://cdn.jsdelivr.net/npm/interactjs@1.3/dist/interact.min.js"></script>
<style>
.nav-pills .nav-link.active, .nav-pills .show>.nav-link{
    background-color: #23c2ff;
    color: white !important;
}

.nav-link{
    color: lightgray !important;
}

.nav-tabs .nav-item.show .nav-link .dekiru, .nav-tabs .nav-link.active{
    color: #ff281e !important;
    background-color: #fff;
    border-color: #dee2e6 #dee2e6 #fff;
}

.submenu-active{
    color: white;
    background-color: green;
}

</style>
</head>

<body>
<div class="container-fuild">
<div class="menu-usern1">
<nav class="navbar navbar-dark bg-primary navbar-expand-md py-md-2">
    <a class="navbar-brand" href="#">FPT DEKIRU</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="nav nav-pills" role="tablist">
    <li class="nav-item">
      <a class="nav-link user-pages" id="home" href="{{ url('users/home') }}">Trang Chủ</a>
    </li>
    <li class="nav-item">
      <a class="nav-link user-pages" id="dekiru" href="{{ url('users/dekiru') }}">Dekiru</a>
    </li>
    <li class="nav-item">
      <a class="nav-link user-pages" id="extend" href="{{ url('users/extend') }}">Mở Rộng</a>
    </li>
  </ul>
</nav>
</div>

</div>

</div>
@yield('content')
<script>
    $( document ).ready(function() {
        
        loadMenuStatus();
        loadStatusDekiruMenu();


        $('.user-pages').click(function(){
            localStorage.menuSelected = this.id;
        });

        $('.dekiru').click(function(){
            localStorage.dekiruMenu = this.id;
        });
        
        
    });

    function loadMenuStatus(){
        if(localStorage.menuSelected == null){
            localStorage.menuSelected = 'home';
        }

        var menuSelected = localStorage.menuSelected;
        
        switch(menuSelected){
            case 'home':
                $('#home').addClass('active');
                $('#dekiru').removeClass('active');
                $('#extend').removeClass('active');
                break;
            case 'dekiru':              
                $('#dekiru').addClass('active');
                $('#home').removeClass('active');
                $('#extend').removeClass('active');
                break;    
            case 'extend':     
                $('#extend').addClass('active');         
                $('#dekiru').removeClass('active');
                $('#home').removeClass('active');               
                break;                        
        }
    }

    function loadStatusDekiruMenu(){
        if(localStorage.dekiruMenu == null){
            localStorage.dekiruMenu = 'vocabulary';
        }

        if(localStorage.dekiruMenu != null){
            var menuSelected = localStorage.dekiruMenu;            
            switch(menuSelected){
                case 'vocabulary':
                    $('#vocabulary').addClass('active');

                    $('#grammar').removeClass('active');
                    $('#kanji').removeClass('active');
                    $('#listen').removeClass('active');
                    $('#speak').removeClass('active');
                    break;
                case 'grammar':
                    $('#grammar').addClass('active');

                    $('#vocabulary').removeClass('active');
                    $('#kanji').removeClass('active');
                    $('#listen').removeClass('active');
                    $('#speak').removeClass('active');
                    break;
                case 'kanji':
                    $('#kanji').addClass('active');

                    $('#vocabulary').removeClass('active');
                    $('#grammar').removeClass('active');
                    $('#listen').removeClass('active');
                    $('#speak').removeClass('active');
                    break;

                case 'listen':
                    $('#listen').addClass('active');

                    $('#vocabulary').removeClass('active');
                    $('#grammar').removeClass('active');
                    $('#kanji').removeClass('active');
                    $('#speak').removeClass('active');
                    break;   

                case 'speak':
                    $('#speak').addClass('active');

                    $('#vocabulary').removeClass('active');
                    $('#grammar').removeClass('active');
                    $('#kanji').removeClass('active');
                    $('#listen').removeClass('active');
                    break;                                     
            }




        }
    }

</script>
</body>



</html>