@extends ('users.layout.user')
@section ('content')

<div class="wrap">
<div class="container">
    
<div class="row">
    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-1.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Hoa anh đào nở rộ khắp Nhật Bản mùa hoa đẹp nhất từ trước đến nay</a>
            </div>
            <div class="new-content">
                <p>Hoa anh đào nở rộ khắp Nhật Bản, mùa hoa đẹp nhất từ trước đến nay!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-2.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Mùa đẹp nhất trong năm của Nhật Bản đã đến</a>
            </div>
            <div class="new-content">
                <p>Rất nhiều khu du lịch nổi tiếng đã mở cửa để đón nhưng du khách ghé thăm Nhật Bản!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-3.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Lễ hội lớn nhất của Nhật Bản, Matsuri đang được tổ chức ở nhiều nơi tại Nhật Bản</a>
            </div>
            <div class="new-content">
                <p>Matsuri là lễ hội được tổ chức ở nhiều nơi tại Nhật Bản và nhiều nhạc công nhất Nhật Bản!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-4.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Dư hấu vuông, một món quà độc đáo của Nhật Bản</a>
            </div>
            <div class="new-content">
                <p>Được trồng tại Shikoku, chỉ với giá 4,5 triệu đồng bạn đã sở hữu món quà đặc biệt này!</p>
            </div>
        </div>
    </div>
</div>

<div class="clear"></div>
<div class="row">
    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-5.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Khu vui chơi lớn nhất Nhật Bản đã mở cửa trở lại</a>
            </div>
            <div class="new-content">
                <p>Tokyo Disneyland đã mở cửa vào xây dựng thêm 2 khu mới đầy hấp dẫn!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-6.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Cá mập của Nhật Bản đang đi đến tuyệt chủng</a>
            </div>
            <div class="new-content">
                <p>Vì lợi ích kinh tế màLoài cá hung dữ đang là món ăn hấp dẫn tại Nhật Bản!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-7.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Nhật Bản tiếp tục rót vốn ODA cho Việt Nam</a>
            </div>
            <div class="new-content">
                <p>Vì lợi ích cùng phát triển cả hai nước Nhật Bản đã đầu tư 1 tỷ USD ODA!</p>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="polaroid">
            <img src="{{ asset ('img/home-8.jpg') }}" alt="Norway" style="width:100%">
            <div class="new-title">
                <a href="#">Nhật Bản tiếp tục rót vốn ODA cho Việt Nam</a>
            </div>
            <div class="new-content">
                <p>Được trồng tại Shikoku, chỉ với giá 4,5 triệu đồng bạn đã sở hữu món quà đặc biệt này!</p>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>




</div>
</div>
@endsection