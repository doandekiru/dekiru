@extends ('users.layout.dekiru')
@section ('dekiru')
<div class="clear-p2"></div>

<div class="row">
<div id="new-word" class="submenu-dekiru submenu-active">
<span>Từ mới</span>
</div>
<div id="practice" class="submenu-dekiru">
<span>Luyện tập</span>
</div>
<div id="test" class="submenu-dekiru">
    <span>Test</span>
</div>
</div>
<div class="clear"></div>

<div class="new-word-c">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Từ Mới
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <audio controls style="width: 100%">
            <source src="{{ asset( 'audio/01-vi-le_03.mp3' ) }}" type="audio/mpeg">
            Your browser does not support the audio element.
        </audio>
        <div class="clear"></div>
        <textarea class="void-text" name="" id="" cols="30" rows="10"></textarea>
        <div class="clear"></div>

    </div>
</div>
</div>

<div class="new-word-l" style="display: none">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Luyện Tập
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div id="practice-1" class="submenu-dekiru submenu-active">
            <span>Ghép Từ</span>
        </div>
        <div id="practice-2" class="submenu-dekiru">
            <span>Ghi Nhớ</span>
        </div>
        <div class="practice-1">
                <div class="clear"></div>
                <div class="draggable-n-c">
                <div id="drag-2" class="drag-drop">
                    <p>わたし</p>
                </div>
                <div id="drag-2" class="drag-drop">
                    <p>しゃいん</p>
                </div>
                <div id="drag-2" class="drag-drop">
                    <p>でんき</p>
                </div>
                <div id="drag-2" class="drag-drop">
                    <p>Tôi</p>
                </div>
                <div id="drag-2" class="drag-drop">
                    <p>Điện, đèn điện</p>
                </div>
                <div id="drag-2" class="drag-drop">
                    <p>Chúng tôi, chúng ta</p>
                </div>
                </div>
            <div class="clear"></div>
        </div>

        <div class="practice-2" style="display: none">
            <div class="clear"></div>
            <textarea class="void-text" name="" id="" cols="30" rows="10"></textarea>
            <div class="clear"></div>
        </div>
    </div>
</div>
</div>

<div class="new-word-t" style="display: none">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Kiểm Tra
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <h3>5 Câu Hỏi Lựa Chọn</h3>
        <div class="clear"></div>
        <textarea class="void-text" name="" id="" cols="30" rows="10"></textarea>
        <div class="clear"></div>

    </div>
</div>
</div>

<script>
    $( document ).ready(function() {
        $('#new-word').click(function(){
            $('#new-word').addClass('submenu-active');
            $('#practice').removeClass('submenu-active');
            $('#test').removeClass('submenu-active');
            $('.new-word-l').hide( "fast" );
            $('.new-word-t').hide( "fast" );
            $('.new-word-c').show( "fast" );
            
        });

        $('#practice').click(function(){
            $('#practice').addClass('submenu-active');
            $('#new-word').removeClass('submenu-active');
            $('#test').removeClass('submenu-active');
            $('.new-word-c').hide( "fast" );
            $('.new-word-t').hide( "fast" );
            $('.new-word-l').show( "fast" );
        });

        $('#test').click(function(){
            $('#test').addClass('submenu-active');
            $('#practice').removeClass('submenu-active');
            $('#new-word').removeClass('submenu-active');
            $('.new-word-c').hide( "fast" );
            $('.new-word-l').hide( "fast" );
            $('.new-word-t').show( "fast" );

        });            
        
        $('#practice-1').click(function(){
            $('#practice-1').addClass('submenu-active');
            $('#practice-2').removeClass('submenu-active');            
            $('.practice-2').hide();
            $('.practice-1').show();
        });  
        
        $('#practice-2').click(function(){
            $('#practice-2').addClass('submenu-active');
            $('#practice-1').removeClass('submenu-active');
            $('.practice-1').hide();
            $('.practice-2').show();
        });  


    });



</script>
<script src="{{ asset ('js/dragging.js') }}"></script>

@endsection
