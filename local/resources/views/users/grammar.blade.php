@extends ('users.layout.dekiru')
@section ('dekiru')
<div class="clear-p2"></div>

<div class="row">
<div id="list" class="submenu-dekiru submenu-active">
<span>Thống kê</span>
</div>
<div id="practice" class="submenu-dekiru">
<span>Luyện tập</span>
</div>
<div id="test" class="submenu-dekiru">
    <span>Test</span>
</div>
</div>
<div class="clear"></div>

<div class="grammar-c">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Thống Kê
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="clear"></div>
        <textarea class="void-text" name="" id="" cols="30" rows="10"></textarea>
        <div class="clear"></div>

    </div>
</div>
</div>

<div class="grammar-l" style="display: none">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Luyện Tập
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<div class="grammar-t" style="display: none">
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                Kiểm Tra
            </div>
            <div class="card-body">
                <div class="car-content-dekiru">
                    <span>Bài 1</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 2</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 3</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 4</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 5</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 6</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 7</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 8</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 9</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 10</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 11</span>
                </div>
                <div class="car-content-dekiru">
                    <span>Bài 12</span>
                </div>
            </div>
        </div>
    </div>

</div>
</div>

<script>
    $( document ).ready(function() {
        $('#list').click(function(){
            $('#list').addClass('submenu-active');
            $('#practice').removeClass('submenu-active');
            $('#test').removeClass('submenu-active');
            $('.grammar-l').hide( "fast" );
            $('.grammar-t').hide( "fast" );
            $('.grammar-c').show( "fast" );
            
        });

        $('#practice').click(function(){
            $('#practice').addClass('submenu-active');
            $('#list').removeClass('submenu-active');
            $('#test').removeClass('submenu-active');
            $('.grammar-c').hide( "fast" );
            $('.grammar-t').hide( "fast" );
            $('.grammar-l').show( "fast" );
        });

        $('#test').click(function(){
            $('#test').addClass('submenu-active');
            $('#practice').removeClass('submenu-active');
            $('#list').removeClass('submenu-active');
            $('.grammar-c').hide( "fast" );
            $('.grammar-l').hide( "fast" );
            $('.grammar-t').show( "fast" );

        });            
        

    });



</script>

@endsection
