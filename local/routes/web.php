<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('users.home');
});

Route::get('users/home', function () {
    return view('users.home');
});

Route::get('users/dekiru', function () {
    return view('users.vocabulary');
});

Route::get('users/extend', function () {
    return view('users.extend');
});

Route::get('users/dekiru/vocabulary', function () {
    return view('users.vocabulary');
});


Route::get('users/dekiru/grammar', function () {
    return view('users.grammar');
});

Route::get('users/dekiru/kanji', function () {
    return view('users.kanji.kanji');
});